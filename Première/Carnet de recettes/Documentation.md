# **DOCUMENTATION DE CARNET DE RECETTES**

## Présentation
*Carnet de recettes* est un programme Python permettant de créer un carnet de recettes de cuisine en toute simplicité. Ses principales fonctionnalités :
- Tri des recettes sucrées et salées
- Affichage de la recette (ingrédient et préparation)
- Écriture de recettes dans un fichier choisi (format json)

## Code source
Vous pouvez accéder au code source de ce programme ici