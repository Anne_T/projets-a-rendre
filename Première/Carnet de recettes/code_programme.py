#!/home/anne/anaconda3/bin/python

# importation des modules tkinter
from tkinter import *
from tkinter.ttk import *
import json
from tkinter.messagebox import *
from tkinter import Button
from tkinter import filedialog

# fonctions des boutons sucré/salé
def sweet():
    """
    affiche les recettes sucrées sous forme de bouton
    """
    for r in recettes:
        if r['genre'].lower() == 'sucré':
            but_sweet=Button(fenetre, text=f"{r['nom']}", width="25", command= lambda r=r: details(r))
            but_sweet.pack()


def salty():
    """
    affiche les recettes salées sous forme de bouton
    """
    for r in recettes:
        if r['genre'].lower() == 'salé':
            but_salty=Button(fenetre, text=f"{r['nom']}",  width="25", command= lambda r=r: details(r))
            but_salty.pack()

def details(recette):
    """
    ouvre une fenêtre avec les ingrédients et la préparation de la recette selectionnée
    """
    fenetre_recette = Toplevel()
    fenetre_recette.title(recette['nom'])

    frame_ingredients = Frame(fenetre_recette, borderwidth=2, relief=GROOVE)
    frame_ingredients.pack(side=LEFT)
    Label(frame_ingredients, text="Ingrédients: ").pack(padx=10, pady=10)
    for r in recette['ingredients']:
        Label(frame_ingredients, text=f"- {r[0]}: {r[1]}").pack(anchor='w', padx=10, pady=10)
    
    frame_preparation = Frame(fenetre_recette, borderwidth=2, relief=GROOVE)
    frame_preparation.pack(side=LEFT)
    Label(frame_preparation, text="Préparation: ").pack(padx=10, pady=10)
    for p in recette['preparation']:
        Label(frame_preparation, text=f"- {p}").pack(anchor='w', padx=10, pady=10)

# fonctions d'ajout de recette
def nouveau():
    """
    ajoute une recette avec le nom, le genre, les ingrédients et la préparation demandés
    """
    nom_new.set("Nom de la recette")
    genre_new.set("Sucré / salé")
    fenetre_nouveau.deiconify()


# fonction d'ajout d'ingrédient
def new_ing():
    """
    ajoute un couple ('ingrédient', quantité) dans list_ingredients
    """
    nom_ingredient.set('Ingrédient')
    quant_ingredient.set(0)
    fenetre_ing.deiconify()

def entrer_ingredient():
    list_ingredients.append((nom_ingredient.get(),quant_ingredient.get()))
    fenetre_ing.withdraw()

# fonction d'ajout d'étape
def new_etape():
    """
    ajoute dans list_etape l'étape notée
    """
    fenetre_etape.deiconify()
    etape.set('Étape')

def entrer_etape():
    list_etape.append(etape.get())
    fenetre_etape.withdraw()

def valider_new():
    new_recette = {}
    new_recette['nom'] = nom_recette.get()
    new_recette['genre'] = genre.get()
    new_recette['ingredients'] = list_ingredients
    new_recette['preparation'] = list_etape
    recettes.append(new_recette)
    fenetre_nouveau.withdraw()


# création du fichier "carnet_de_recettes.json" contenant la liste "recettes"
def charger_recettes():
    fichier_recettes = filedialog.askopenfilename(title='Choisir un fichier de recettes', filetypes=(('fichier recettes', '*.json'),))
    if fichier_recettes:
        fichier = open(fichier_recettes,'rb')
        recettes = json.load(fichier)
        fichier.close()
        return recettes
    else:
        return []

def enregistrer_recettes():
    fichier_recettes = filedialog.askopenfilename(title='Choisir un fichier de recettes', filetypes=(('fichier recettes', '*.json'),))
    if fichier_recettes:   
        fichier = open(fichier_recettes,'w')
        json.dump(recettes,fichier)
        fichier.close()

# création de la fenetre tkinter
fenetre = Tk()
fenetre.title("Livre de recettes")
fenetre.geometry("300x200")
fenetre.configure(background='white')

recettes = []
list_ingredients = []
list_etape = []


# fenetre de création d'une nouvelle recette
fenetre_nouveau = Toplevel()
fenetre_nouveau.withdraw()
fenetre_nouveau.configure(background='white')
fenetre_nouveau.title("Ajouter une recette")

Label(fenetre_nouveau, text='Nom : ', background='white', foreground='black').pack()
nom_new = StringVar() 
nom_recette = Entry(fenetre_nouveau, textvariable=nom_new, width=30)
nom_recette.pack()

Label(fenetre_nouveau, text='Sucré ou salé : ', background='white', foreground='black').pack()
genre_new = StringVar()
genre = Entry(fenetre_nouveau, textvariable=genre_new, width=30)
genre.pack()

but_ing =Button(fenetre_nouveau, text="Ajouter un ingrédient", background='white', command= new_ing)
but_ing.pack()

but_prepa =Button(fenetre_nouveau, text='Ajouter une étape de préparation', background='white',command= new_etape)
but_prepa.pack()

but_valider =Button(fenetre_nouveau, text='Valider', command= valider_new).pack()

# fenetre nouvelle étape
fenetre_etape = Toplevel()
fenetre_etape.withdraw()
fenetre_etape.config(background='white')
fenetre_etape.title('Ajouter une étape')

Label(fenetre_etape, text='Étape à ajouter : ', background='white', foreground='black').pack()

etape = StringVar()
new_etape = Entry(fenetre_etape, textvariable=etape, width=30)
new_etape.pack()

but_ajout = Button(fenetre_etape, text="Ajouter", command= entrer_etape)
but_ajout.pack()

# fenetre nouvel ingrédient
fenetre_ing = Toplevel()
fenetre_ing.withdraw()
fenetre_ing.configure(background='white')
fenetre_ing.title('Ajouter un ingrédient')

Label(fenetre_ing, text='Ingrédient à ajouter : ', background='white', foreground='black').pack()
nom_ingredient = StringVar()
ing = Entry(fenetre_ing, textvariable=nom_ingredient, width=30)
ing.pack()

Label(fenetre_ing, text='Quantité : ', background='white', foreground='black').pack()
quant_ingredient = IntVar()
qtt = Entry(fenetre_ing, textvariable=quant_ingredient, width=30)
qtt.pack()

but_add = Button(fenetre_ing, text="Ajouter", command= entrer_ingredient)
but_add.pack()

# création de la barre de menu
barre_recettes = Menu(fenetre)
barre_recettes.configure(background='black', foreground='white', activebackground='white', activeforeground='black')

menu1 = Menu(barre_recettes, tearoff=0)
menu1.config(background='black', foreground='white', activebackground='white', activeforeground='black')
menu1.add_command(label="Sucré", command=sweet)
menu1.add_command(label="Salé", command=salty)

barre_recettes.add_cascade(label="Recettes", menu=menu1)

menu2 = Menu(barre_recettes, tearoff=0)
menu2.config(background='black', foreground='white', activebackground='white', activeforeground='black')
menu2.add_command(label="Nouvelle recette", command=nouveau)
menu2.add_separator()
menu2.add_command(label="Sauvegarder", command=enregistrer_recettes)
barre_recettes.add_cascade(label="Modifier", menu=menu2)

menu3 = Menu(barre_recettes, tearoff=0)
menu3.config(background='black', foreground='white', activebackground='white', activeforeground='black')
menu3.add_command(label="Documentation", command=fenetre.quit)
menu3.add_command(label="Vidéo", command=fenetre.quit)
barre_recettes.add_cascade(label="Aide", menu=menu3)

fenetre.config(menu=barre_recettes)

recettes = charger_recettes()

fenetre.mainloop()