RÉALISER UNE OEUVRE ARTISTIQUE AVEC informARTique.py
======

*par Anne Touvet*

![exemple de dessin réalisé avec informARTique.py](exemple.png)

###Présentation du programme

informARTique.py est un programme Python qui réalise aléatoirement une oeuvre artistique. Il utilise le module [turtle.py](https://docs.python.org/3/library/turtle.html).

Ce qui change aléatoirement :
- La couleur de la tortue
- La taille de la tortue
- L'endroit où va la tortue sur le canva

###Exemple d'utilisation

```python
import InformARTique as inf

tableau()
```
![dessin réalisé avec informARTique.py](exemple2.png)


###Dépendances
*celles-ci devront être installées au préalable*

- [*Python 3*](https://www.python.org/download/releases/3.0/)
- [*Random*](https://docs.python.org/3/library/random.html)
- [*Turtle*](https://docs.python.org/fr/3/library/turtle.html)

###Importer le module

Pour utiliser le programme, vous pouvez lancer le fichier depuis un terminal :
```zsh
python3 -i InformARTique.py
```

ou l'importer depuis python:
```python
import InformARTique
```
```python
from InformARTique import *
```

###Utiliser le module

- Constantes du module

| Nom de la constante | Type | Valeur par défaut | Description |
|:-------------------:|:----:|:-----------------:|:-----------:|
| `HAUTEUR` | `int` | `500` | Hauteur du canva |
| `LARGEUR` | `int` | `500` | Largeur du canva |

- Fonctions du module

####`couleurs`

La fonction `couleurs` renvoie une liste de trois chiffres aléatoires. Elle ne prend pas d'argument.

####`stayin`

La fonction `stayin` oblige la tortue à rester dans le canva.

| Argument | Explication |
|:--------:|:-----------:|
| x | coordonnée x de la position de la tortue |
| y | coordonnée y de la position de la tortue |
| ep | épaisseur du trait de la tortue |

####`mouvement`

La fonction `mouvement` dessine aléatoirement un nombre de traits aléatoire d'une épaisseur aléatoire. Elle ne prend pas d'argument.

####`tableau`

La fonction `tableau` est la fonction principale du programme. Elle dessine l'oeuvre artistique dans un canva de hauteur `HAUTEUR` et de largeur `LARGEUR`.

