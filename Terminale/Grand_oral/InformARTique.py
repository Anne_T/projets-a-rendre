import turtle as tu
import random


#tailles de la toile:
HAUTEUR = 500
LARGEUR = 500


def couleurs():
    """
    crée une couleur aléatoire
    """
    return [random.random() for i in range(3)]


def stayin(x,y,ep): #A REVOIR
    if x+ep//2 > LARGEUR//2:
        x -= ep
        tu.goto(x,y)
        print(F"x={x}, y={y} -> droite")
    elif x-ep//2 < -LARGEUR//2:
        x += ep
        tu.goto(x,y)
        print(F"x={x}, y={y} -> gauche")
    elif y+ep > HAUTEUR//2:
        y -= ep
        tu.goto(x, y)
        print(F"x={x}, y={y} -> haut")
    elif y-ep < -HAUTEUR//2:
        y += ep
        tu.goto(x, y)
        print(F"x={x}, y={y} -> bas")


def mouvement():
    for i in range(random.randint(0,100)):
        epaisseur = random.randint(0,40)
        x,y = tu.pos()
        stayin(x,y,epaisseur)
        tu.pensize(epaisseur)
        tu.forward(epaisseur)
        random.choice([tu.rt(random.randint(0,180)), tu.lt(random.randint(0,180))])


def tableau():
    tu.setup(LARGEUR, HAUTEUR)
    tu.title('informARTique')
    tu.ht()
    tu.speed(0)
    tu.pendown()
    nb_mouvement = random.randint(0,50)
    for i in range(nb_mouvement):
        tu.pendown()
        tu.color(couleurs())
        mouvement()
        tu.penup()
        tu.goto(random.randint(-HAUTEUR//2, HAUTEUR//2), random.randint(-LARGEUR//2, LARGEUR//2))
    tu.done()


tableau()
