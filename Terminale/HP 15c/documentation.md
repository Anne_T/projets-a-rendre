# DOCUMENTATION SIMULATION HP 15C

## PRÉSENTATION

***Simulation HP 15c*** est une simulation de la calculatrice HP15C. Elle permet ainsi de réaliser un calcul sous la forme d'écriture polonaise inversée. Pour en savoir plus sur la calculatrice HP 15c, vous pouvez vous renseigner [ici](https://fr.wikipedia.org/wiki/HP-15C) et pour comprendre comment fonctionne l'écriture polonaise inversée, c'est [ici](https://fr.wikipedia.org/wiki/Notation_polonaise_inverse).

**ATTENTION** : La calculatrice de ce programme **n'est pas** une véritable HP 15c dans le sens où elle n'offre pas toutes les possibilités de la HP 15c (tel que le travail sur les matrices). Elle ne peut que calculer des nombres entiers ou décimaux sous forme d'écriture polonaise inversée.


### OPÉRATIONS POSSIBLES:
- Additions
- Soustractions
- Multiplications
- Divisions entières
- Divisions décimales
- Puissances


## UTILISATION

1. Pour utiliser *HP 15c*, commencez par télécharger l'ensemble de [ce dossier](https://gitlab.com/Anne_T/projets-a-rendre/-/tree/master/Terminale/HP%2015c#). 

2. Lancez le fichier intitulé "hp15c.py"

3. Entrez votre calcul en appuyant sur la touche "entrer" pour valider à chaque nombre ou opération. Pour demander le résultat, appuyez une nouvelle fois sur "entrer". Vous pouvez à tout moment arrêter le programme en écrivant "stop".


## MODIFICATIONS

- Code source : https://gitlab.com/Anne_T/projets-a-rendre/-/tree/master/Terminale/HP%2015c#