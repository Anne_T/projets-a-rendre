from listeC import Maillon
import operator

class Pile:
    """
    Implémentation du type abstrait pile avec des objets de type list
    """

    def __init__(self):
        self._sommet = None #la pile est vide par défaut

    def est_vide(self):
        return self._sommet is None

    def empiler(self, val):
        s = Maillon(val) #on ajoute val à la pile
        if self.est_vide():
            self._sommet = s
        else:
            s.set_suiv(self._sommet)
            self._sommet = s

    def depiler(self: "Pile"):
        if self.est_vide():
            raise IndexError("La pile est vide et ne peut pas être dépilée")
        else:
            s = self._sommet.get_val()
            self._sommet = self._sommet.get_suiv()
            return s

    def nonepop(self: "Pile"):
        """
        enlève le dernier élément de la pile et renvoie cet élement, si elle est vide, renvoie None
        """
        if self.est_vide():
            return None
        else:
            return self.depiler()

    def duplique(self: "Pile"):
        """
        ajoute à la pile un nouvel élément ayant le même type et la même valeur que le dernier élément ajouté
        """
        sval = self._sommet.get_val()
        self.empiler(sval)
    
    def echange(self: "Pile"):
        """
        échange les deux derniers éléments de la Pile
        """
        val1 = self.depiler()
        val2 = self.depiler()
        self.empiler(val1)
        self.empiler(val2)

    def ecrase(self: "Pile"):
        """
        efface tout ce qui est dans la pile
        """
        while not self.est_vide():
            self.depiler()

    def inverse(self: "Pile"):
        """
        inverse tous les éléments de la pile
        """
        new_pile = Pile()
        while not self.est_vide():
            new_pile.empiler(self.nonepop())
        return new_pile

    def copy(self):
        """
        crée une copie de la pile
        """
        new_pile = Pile()
        copie_pile = Pile()
        while not self.est_vide():
            new_pile.empiler(self.nonepop())
        while not new_pile.est_vide():
            el = new_pile.nonepop()
            self.empiler(el)
            copie_pile.empiler(el)
        return copie_pile

    def longueur(self):
        """
        renvoie le nombre d'éléments de la pile
        """
        cop_pile = self.copy()
        nombre = 0
        while not cop_pile.est_vide():
            cop_pile.nonepop()
            nombre += 1
        return nombre

    def liste(self):
        """
        renvoie une liste qui contient les éléments de la pile
        """
        new_pile = self.copy()
        liste = []
        while not new_pile.est_vide():
            liste.append(new_pile.nonepop())
        return liste

    def __repr__(self):
        return f"{self._sommet.get_val()}|-"