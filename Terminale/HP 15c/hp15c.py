from pile import Pile
import operator


OPERATIONS = {
    '+':  operator.add, '-':  operator.sub,
    '*':  operator.mul, '/':  operator.truediv,
    '%':  operator.mod, '**': operator.pow,
    '//': operator.floordiv
}

def npi()->int:
    """
    calcule les éléments donnés en input sous la forme d'écriture polonaise inversée
    """
    p = Pile()
    nombres = 0
    fin = False
    while not fin:
        element = input('Entrez un nombre ou un opérateur (ou "entrer" pour terminer): ')
        if element == '':
            if p.longueur() != 1:
                print(f"Votre calcul n'est pas fini : {p.liste()} \n(Si vous souhaitez tout de même arrêter le calcul, écrivez STOP)")
            else:
                print(f'Résultat : {p}')
                fin = True
        elif element.upper() == "STOP":
            return print("STOP")
        else :
            if element in OPERATIONS:
                if nombres == 1:
                    raise ArithmeticError("Vous essayez de calculer un nombre avec rien !")
                el1 = float(p.nonepop())
                el2 = float(p.nonepop())
                opera = OPERATIONS[element]
                resultat = opera(el1, el2)
                p.empiler(resultat)
                nombres -= 1
                print(p.liste())
            else:
                p.empiler(float(element))
                nombres += 1
                print(p.liste())

npi()