import turtle as tu
import math
import random
from random import randint

FENETRES = 30
LARG_P_FENETRE = 30
LONG_P_FENETRE = 50
LARG_PORTE = 30
LONG_PORTE = 50
LARG_TOIT = 10
LONG_TOIT = 140

def fenetres(cotes):
    """
    Trace un carré de côté "cotes"
    """
    tu.forward(12)
    tu.fillcolor('white')
    tu.begin_fill()
    tu.down()
    for i in range(4):
        tu.forward(cotes)
        tu.right(90)
    tu.end_fill()
    tu.up()
    tu.forward(cotes)

def porte(long, larg):
    """
    Trace un rectangle de longueur "long" et de largeur "larg"
    """
    tu.fillcolor('grey')
    tu.forward(12)
    tu.begin_fill()
    for i in range(2):
        tu.down()
        tu.forward(larg)
        tu.right(90)
        tu.forward(long)
        tu.right(90)
    tu.end_fill()
    tu.up()
    tu.forward(larg)

def p_fenetre(long, larg):
    """
    Dessine des rectangles de "long" de longueur et de "larg" de largeur
    """
    tu.forward(12)
    tu.fillcolor('white')
    tu.begin_fill()
    for i in range(2):
        tu.down()
        tu.forward(larg)
        tu.right(90)
        tu.forward(long)
        tu.right(90)
    tu.end_fill()
    tu.up()
    #deplacement pour le balcon
    tu.right(90)
    tu.forward(long-long/3)
    tu.right(90)
    tu.forward(larg/5)
    tu.right(180)
    balcon(long/3, larg/5)

def balcon(longueur, largeur):
    """
    trace 7 rectangles collés
    """
    tu.down()
    for i in range(7):
        for j in range(2):
            tu.forward(largeur)
            tu.right(90)
            tu.forward(longueur)
            tu.right(90)
        tu.forward(largeur)
    tu.backward(largeur)
    tu.left(90)
    tu.forward(longueur*2)
    tu.right(90)
    tu.up()

def toit(long, larg):
    """
    Trace un rectangle de longueur "long" et de largeur "larg"
    """
    tu.fillcolor('black')
    tu.begin_fill()
    for i in range(2):
        tu.down()
        tu.forward(long)
        tu.left(90)
        tu.forward(larg)
        tu.left(90)
    tu.end_fill()
    tu.up()
    tu.forward(long)

def fenetre_p_fenetre(total):
    """
    renvoie aleatoirement à la fonction "fenetre" ou "p_fenetre"
    """
    for i in range(total):
        ouverture = randint(0,1)
        if ouverture == 0:
            p_fenetre(LONG_P_FENETRE, LARG_P_FENETRE)
        else:
            fenetres(FENETRES)
    
def etages(couleur):
    """
    Trace un rectangle de la couleur donnée en argument
    """
    tu.fillcolor(couleur)
    tu.begin_fill()
    for i in range(2):
        tu.down()
        tu.forward(140)
        tu.left(90)
        tu.forward(60)
        tu.left(90)
    tu.end_fill()
    tu.up()
    tu.left(90)
    tu.forward(50)
    tu.right(90)

    fenetre_p_fenetre(3)

    tu.forward(14)
    tu.left(90)
    tu.forward(10)
    tu.left(90)
    tu.forward(140)
    tu.right(180)


def rdc(couleur):
    """
    Trace un rectangle de la couleur donnée en argument avec des fenêtres et/ou des portes fenêtres et une porte
    """
    tu.fillcolor(couleur)
    tu.begin_fill()
    for i in range(2):
        tu.down()
        tu.forward(140)
        tu.left(90)
        tu.forward(60)
        tu.left(90)
    tu.end_fill()
    tu.up()
    tu.left(90)
    tu.forward(50)
    tu.right(90)

    emplacement = randint(0,2)
    if emplacement == 0:
        porte(LONG_PORTE, LARG_PORTE)
        fenetre_p_fenetre(2)
    elif emplacement == 1:
        fenetre_p_fenetre(1)
        porte(LONG_PORTE, LARG_PORTE)
        fenetre_p_fenetre(1)
    else:
        fenetre_p_fenetre(2)
        porte(LONG_PORTE, LARG_PORTE)

    tu.forward(14)
    tu.left(90)
    tu.forward(10)
    tu.left(90)
    tu.forward(140)
    tu.right(180)

def couleurs():
    """
    assigne à "farben" une couleur aléatoire
    """
    farben = []
    for i in range(3):
        farben.append(random.random())
    return farben

def immeubles():
    tu.ht()
    tu.speed(0)
    tu.up()
    tu.goto(-300, -150)
    tu.down()
    for i in range(4):
        clr = couleurs()
        rdc(clr)
        nbr_etages = randint(0,4)
        for j in range(nbr_etages):
            etages(clr)
        toit(LONG_TOIT, LARG_TOIT)
        tu.right(90)
        tu.forward((nbr_etages+1)*60)
        tu.left(90)
        tu.forward(20)

immeubles()