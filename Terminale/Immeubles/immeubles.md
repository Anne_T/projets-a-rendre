# **DOCUMENTATION DE IMMEUBLES**

## PRÉSENTATION
*Immeubles* est un programme Python qui dessine une rue d'immeubles composés:
- d'un rez-de-chaussée contenant une porte ainsi que des fenêtres ou des portes-fenêtres
- d'un nombre d'étages aléatoire inférieur à 5 contenant un nombre de portes et de portes-fenêtres aléatoire.

*Immeubles* utilise le module "Turtle".

## UTILISATION

### - Télécharger le programme

*Immeubles* est un programme Python. Pour l'utiliser, téléchargez ce [dossier](https://gitlab.com/Anne_T/immeubles) et ouvrir le fichier intitulé "prog_immeuble.py" dans un éditeur de code tel que VsCode.

### - Lancer le programme

Il ne vous reste plus qu'à lancer le programme, la suite se fait toute seule.

## MODIFICATIONS

### - Code source
Le code du programme est accessible ici : https://gitlab.com/Anne_T/immeubles/-/blob/master/prog_immeuble.py.


